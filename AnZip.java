import java.io.*;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Разархивация zip архива
 * запрашивает у пользователя путь к архиву и имя
 */
public class AnZip {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь к архиву");
        String path = scanner.nextLine();//"C://Games//
        System.out.println("Введите имя архива");
        unpackZip(path, scanner.nextLine());//"zip"
    }

    /**
     * @param path    путь к архиву
     * @param zipname имя архива
     */
    private static void unpackZip(String path, String zipname) {
        long t1 = System.currentTimeMillis();
        InputStream is;
        ZipInputStream zis;
        try {
            is = new FileInputStream(path + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is)); //Для чтения архивов применяется класс
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;
                String filename = ze.getName();
                if (ze.isDirectory()) {////объект является каталогом
                    File directPath = new File(path + zipname.substring(0,
                            zipname.length() - 4) + "\\" + filename);
                    directPath.mkdirs();// создаёт сам каталог, так и всех его родителей
                } else {
                    File directPath = new File(path + zipname.substring(0,
                            zipname.length() - 4) + "\\");
                    directPath.mkdir();
                    FileOutputStream fout = new FileOutputStream(path +
                            zipname.substring(0, zipname.length() - 4) + "\\" + filename);
                    while ((count = zis.read(buffer)) != -1) {
                        baos.write(buffer, 0, count);
                        byte[] bytes = baos.toByteArray();
                        fout.write(bytes);
                        baos.reset();
                    }
                    fout.close();
                    zis.closeEntry();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("разархивация завершена \nвремя работы = " +
                (System.currentTimeMillis() - t1) / 1000 + " cек");
    }
}
