import java.io.*;
import java.net.URI;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * архивация файла
 * запрашивает у пользователя путь к папке и место назначения с имя архива
 */

public class Zip {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь к папке архивации");
        File dir1 = new File(scanner.nextLine());//"C://Games//test"
        System.out.println("Введите место назначения и имя архива");
        pack(dir1, scanner.nextLine());//"C://Games//zip"
    }

    /**
     * @param directory путь к папке
     * @param to        место назначения с имя архива
     */
    private static void pack(File directory, String to) throws IOException {
        long t1 = System.currentTimeMillis();
        System.out.println("начало архивации");
        URI base = directory.toURI();
        Deque<File> queue = new LinkedList<>();
        queue.push(directory);
        OutputStream out = new FileOutputStream(new File(to + ".zip"));
        Closeable res = out;

        try (ZipOutputStream zipOutputStream = new ZipOutputStream(out)) {
            res = zipOutputStream;
            while (!queue.isEmpty()) {//проверка является queue пустым
                directory = queue.pop();
                for (File child : directory.listFiles()) {//возвращает массив файлов и подкаталогов, которые находятся в определенном каталоге
                    String name = base.relativize(child.toURI()).getPath();//Создает URI <tt> file: </ tt>, который представляет этот абстрактный путь.
                    //преобразует этот абстрактный путь в строку пути
                    //получить новый путь назначения
                    if (child.isDirectory()) {//объект является каталогом
                        queue.push(child);
                        name = name.endsWith("/") ? name : name + "/";//если последовательность символов, представленного аргумента является окончанием последовательности символов
                        zipOutputStream.putNextEntry(new ZipEntry(name));// Начинает писать новую запись файла ZIP и располагает поток в запуск данных записи.
                    } else {
                        zipOutputStream.putNextEntry(new ZipEntry(name));
                        try (InputStream in = new FileInputStream(child)) {
                            byte[] buffer = new byte[1024];
                            while (in.read(buffer) < 0) {
                                zipOutputStream.write(buffer, 0, in.read(buffer));
                                //* Записывает массив байтов в текущие данные записи ZIP. Этот метод
                                //* будет блокироваться до тех пор, пока не будут записаны все байты.
                            }
                        }
                    }
                }
            }
            System.out.println("Архивация завершена \nвремя работы = " +
                    (System.currentTimeMillis() - t1) / 1000 + " cек");
        } finally {
            res.close();
        }
    }
}
